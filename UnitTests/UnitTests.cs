﻿using System;
using System.Collections.Generic;
using System.Linq;
using SampleQueries;
using Task.Data;
using Xunit;

namespace UnitTests
{
	public class UnitTests
	{
		private readonly LinqSamples _service;

		public UnitTests()
		{
			_service = new LinqSamples();
		}

		public class Linq1UnitTests : UnitTests
		{
			[Theory]
			[InlineData(5, 63)]
			[InlineData(0, 89)]
			[InlineData(-1, 91)]
			public void Linq1_GetCustomers(int minCounter, int expectedCount)
			{
				var actualResult = new List<Customer>(_service.Linq_1(minCounter));
				Assert.Equal(expectedCount, actualResult.Count);
			}
		}

		public class Linq2UnitTests : UnitTests
		{
			[Theory]
			[InlineData(91)]
			public void Linq2_GetData(int expectedCount)
			{
				var actualResult = _service.Linq_2();
				Assert.Equal(expectedCount, actualResult.Count());
			}

			[Fact]
			public void Linq2_GetDataGroupBy()
			{
				var expectedResult = _service.Linq_2().ToList();
				var actualResult = _service.Linq_2GroupBy().ToList();

				Assert.Equal(expectedResult.Count, actualResult.Count);
				for (int i = 0; i < expectedResult.Count; i++)
				{
					Assert.Equal(expectedResult[i].Item1.Address, actualResult[i].Item1.Address);
					Assert.Equal(expectedResult[i].Item1.City, actualResult[i].Item1.City);
					Assert.Equal(expectedResult[i].Item1.CompanyName, actualResult[i].Item1.CompanyName);
					Assert.Equal(expectedResult[i].Item1.Country, actualResult[i].Item1.Country);
					Assert.Equal(expectedResult[i].Item1.CustomerID, actualResult[i].Item1.CustomerID);
					Assert.Equal(expectedResult[i].Item1.Fax, actualResult[i].Item1.Fax);
					Assert.Equal(expectedResult[i].Item1.Phone, actualResult[i].Item1.Phone);
					Assert.Equal(expectedResult[i].Item1.PostalCode, actualResult[i].Item1.PostalCode);
					Assert.Equal(expectedResult[i].Item1.Region, actualResult[i].Item1.Region);

					Assert.Equal(expectedResult[i].Item2.Count(), actualResult[i].Item2.Count());
					var expectedSuppliers = expectedResult[i].Item2.ToList();
					var actualSuppliers = actualResult[i].Item2.ToList();

					for (int j = 0; j < expectedSuppliers.Count; j++)
					{
						Assert.Equal(expectedSuppliers[j].Address, actualSuppliers[j].Address);
						Assert.Equal(expectedSuppliers[j].City, actualSuppliers[j].City);
						Assert.Equal(expectedSuppliers[j].Country, actualSuppliers[j].Country);
						Assert.Equal(expectedSuppliers[j].SupplierName, actualSuppliers[j].SupplierName);
					}
				}
			}

		}

		public class Linq3UnitTests : UnitTests
		{
			[Theory]
			[InlineData(0, 89)]
			[InlineData(-1, 89)]
			[InlineData(50000, 0)]
			public void Linq3_GetCustomers(int minOrder, int expectedResult)
			{
				var actualResult = _service.Linq_3(minOrder);
				Assert.Equal(expectedResult, actualResult.Count());
			}
		}

		public class Linq4UnitTests : UnitTests
		{
			[Theory]
			[InlineData(0, "8/25/1997")]
			[InlineData(21, "1/1/0001")]
			[InlineData(90, "12/5/1996")]
			public void Linq4_GetCustomers(int position, string expectedDate)
			{
				var actualResult = _service.Linq_4();
				Assert.Equal(91, actualResult.Count());
				Assert.Equal(actualResult.ElementAt(position).Item2, DateTime.Parse(expectedDate));
			}
		}

		public class Linq5UnitTests : UnitTests
		{
			[Theory]
			[InlineData(0, "1/29/1998")]
			[InlineData(88, "7/18/1996")]
			public void Linq5_GetCustomers(int position, string expectedDate)
			{
				var actualResult = _service.Linq_5();
				Assert.Equal(89, actualResult.Count());
				Assert.Equal(actualResult.ElementAt(position).Item2, DateTime.Parse(expectedDate));
			}
		}

		public class Linq6UnitTests : UnitTests
		{
			[Theory]
			[InlineData(74)]
			public void Linq6_GetCustomers(int expectedResult)
			{
				var actualResult = _service.Linq_6();
				Assert.Equal(expectedResult, actualResult.Count());
			}
		}

		public class Linq7UnitTests : UnitTests
		{
			[Theory]
			[InlineData(8)]
			public void Linq7_GetCustomers(int expectedResult)
			{
				var actualResult = _service.Linq_7();
				Assert.Equal(expectedResult, actualResult.Count());
			}
		}

		public class Linq8UnitTests : UnitTests
		{
			[Theory]
			[InlineData(20, 50, 3)]
			[InlineData(0, 0, 1)]
			[InlineData(-1, -1, 1)]
			public void Linq8_GetCustomers(int lowAverage, int expensiveAverange, int expectedResult)
			{
				var actualResult = _service.Linq_8(lowAverage, expensiveAverange);
				Assert.Equal(expectedResult, actualResult.Count());
			}
		}

		public class Linq9UnitTests : UnitTests
		{
			[Theory]
			[InlineData(69)]
			public void Linq9_GetCustomers(int expectedResult)
			{
				var actualResult = _service.Linq_9();
				Assert.Equal(expectedResult, actualResult.Count());
			}
		}

		public class Linq10UnitTests : UnitTests
		{
			[Theory]
			[InlineData(91)]
			public void Linq10_GetStatistic(int expectedResult)
			{
				var actuaResult = _service.Linq_10();
				Assert.Equal(expectedResult, actuaResult.Count());
			}
		}

	}
}