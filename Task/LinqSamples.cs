﻿// Copyright © Microsoft Corporation.  All Rights Reserved.
// This code released under the terms of the 
// Microsoft Public License (MS-PL, http://opensource.org/licenses/ms-pl.html.)
//
//Copyright (C) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using SampleSupport;
using Task.Data;

// Version Mad01

namespace SampleQueries
{
	[Title("LINQ Module")]
	[Prefix("Linq")]
	public class LinqSamples : SampleHarness
	{

		private DataSource dataSource = new DataSource();

		public IEnumerable<Customer> Linq_1(int minimumOrderCouner)
		{
			return dataSource.Customers
				.Where(c => c.Orders.Length > minimumOrderCouner)
				.Select(c => c);
		}

		public IEnumerable<Tuple<Customer, IEnumerable<Supplier>>> Linq_2()
		{
			return dataSource.Customers.Select(
				c => new Tuple<Customer, IEnumerable<Supplier>>(c, dataSource.Suppliers
					.Where(s => s.Country == c.Country && s.City == c.City)
				));
		}

		public IEnumerable<Tuple<Customer, IEnumerable<Supplier>>> Linq_2GroupBy()
		{
			var result = dataSource.Customers.GroupJoin(dataSource.Suppliers,
				c => new { c.City, c.Country },
				s => new { s.City, s.Country },
				(c, s) => new { Customer = c, Suppliers = s }).Select(c => c).AsEnumerable();

			return result.Select(item => new Tuple<Customer, IEnumerable<Supplier>>(item.Customer, item.Suppliers));
		}

		public IEnumerable<Customer> Linq_3(int minOrderSum)
		{
			return dataSource.Customers.Where(c => c.Orders.Any(s => s.Total > minOrderSum)).Select(c => c);
		}

		public IEnumerable<Tuple<Customer, DateTime>> Linq_4()
		{
			var data = dataSource.Customers.Select(
				c => new Tuple<Customer, IEnumerable<Order>>(c, c.Orders.OrderBy(o => o.OrderDate))
				);

			var result = new List<Tuple<Customer, DateTime>>();
			foreach (var item in data)
			{
				if (!item.Item2.Any())
				{
					result.Add(new Tuple<Customer, DateTime>(item.Item1, default));
				}
				else
				{
					result.Add(new Tuple<Customer, DateTime>(item.Item1, item.Item2.First().OrderDate));
				}
			}
			return result;
		}

		public IEnumerable<Tuple<Customer, DateTime>> Linq_5()
		{
			return (dataSource.Customers.Where(c => c.Orders.Any())
				.Select(c => new
				{
					Cusomer = c,
					StartDate = c.Orders.OrderBy(o => o.OrderDate).Select(o => o.OrderDate).First(),
					TotalSum = c.Orders.Sum(o => o.Total)
				}).OrderByDescending(c => c.StartDate.Year)
				.ThenByDescending(c => c.StartDate.Month)
				.ThenByDescending(c => c.TotalSum).Select(item => new Tuple<Customer, DateTime>(item.Cusomer, item.StartDate)));
		}

		public IEnumerable<Customer> Linq_6()
		{
			return dataSource.Customers.Where(
			   c => c.PostalCode != null && c.PostalCode.Any(sym => sym < '0' || sym > '9')
				   || string.IsNullOrWhiteSpace(c.Region)
				   || c.Phone.FirstOrDefault() != '(');
		}

		public IEnumerable<Tuple<string, IEnumerable<Tuple<bool, IEnumerable<Product>>>>> Linq_7()
		{
			return from item in dataSource.Products
				.GroupBy(p => p.Category)
				.Select(g => new
				{
					Category = g.Key,
					ProductsByStock = g.GroupBy(p => p.UnitsInStock > 0)
						.Select(a => new
						{
							HasInStock = a.Key,
							Products = a.OrderBy(prod => prod.UnitPrice).Select(b => b)
						}).Select(a => a)
				})
				   let list = item.ProductsByStock.Select(item2 => new Tuple<bool, IEnumerable<Product>>(item2.HasInStock, item2.Products))
				   select new Tuple<string, IEnumerable<Tuple<bool, IEnumerable<Product>>>>(item.Category, list);
		}

		public IEnumerable<Tuple<string, Product>> Linq_8(int lowAverage, int expensiveAverage)
		{
			return dataSource.Products
				.GroupBy(p => p.UnitPrice < lowAverage ? "Cheap"
					: p.UnitPrice < expensiveAverage ? "Average price" : "Expensive")
				.Select(item => new Tuple<string, Product>(item.Key, item.First()));
		}

		public IEnumerable<Tuple<string, Tuple<double, decimal>>> Linq_9()
		{
			return dataSource.Customers
				.GroupBy(c => c.City).AsEnumerable()
				.Select(c => new Tuple<string, Tuple<double, decimal>>
				(
					c.Key,
					new Tuple<double, decimal>
					(
						c.Average(p => p.Orders.Length),
						c.Average(p => p.Orders.Sum(o => o.Total)
						)
					)
				)
			);
		}

		public IEnumerable<Statistic> Linq_10()
		{
			return dataSource.Customers
				.Select(c => new
				{
					c.CustomerID,
					MonthsStatistic = c.Orders.GroupBy(o => o.OrderDate.Month)
										.Select(g => new { Month = g.Key, OrdersCount = g.Count() }),
					YearsStatistic = c.Orders.GroupBy(o => o.OrderDate.Year)
										.Select(g => new { Year = g.Key, OrdersCount = g.Count() }),
					YearMonthStatistic = c.Orders
										.GroupBy(o => new { o.OrderDate.Year, o.OrderDate.Month })
										.Select(g => new { g.Key.Year, g.Key.Month, OrdersCount = g.Count() })
				}).Select(item => new Statistic
				{
					CustomerId = item.CustomerID,
					MonthsStatistic = item.MonthsStatistic.Select(s => new Tuple<int, int>(s.Month, s.OrdersCount)),
					YearsStatistic = item.YearsStatistic.Select(s => new Tuple<int, int>(s.Year, s.OrdersCount)),
					YearMonthStatistic = item.YearMonthStatistic.Select(s => new Tuple<int, int, int>(s.Year, s.Month, s.OrdersCount))
				});
		}
	}
}