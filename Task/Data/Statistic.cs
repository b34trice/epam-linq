﻿using System;
using System.Collections.Generic;

namespace Task.Data
{
	public class Statistic
	{
		public string CustomerId { get; set; }
		public IEnumerable<Tuple<int, int>> MonthsStatistic { get; set; }
		public IEnumerable<Tuple<int, int>> YearsStatistic { get; set; }
		public IEnumerable<Tuple<int, int, int>> YearMonthStatistic { get; set; }
	}
}
